import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, ActivatedRouteSnapshot } from '@angular/router';

import{HomeComponent,
NavigationBarComponent}from './HomePage/index';
import{LoginComponent}from './Login/index'
import { appRoutes } from './route';
import { PhotoAppComponent } from './photgallary.component';
import { AccountComponent } from './account/create.account.component';

@NgModule({
  imports:      [ BrowserModule,
                  RouterModule.forRoot(appRoutes) 
                ],
  declarations: [ 
                  PhotoAppComponent,
                  HomeComponent,
                  NavigationBarComponent,
                  LoginComponent,
                  AccountComponent
  ],
  bootstrap:    [ PhotoAppComponent ]
})
export class AppModule { }
