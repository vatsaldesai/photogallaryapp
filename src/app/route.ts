import { Routes } from "@angular/router";
import {  LoginComponent } from './Login/index'
import { HomeComponent } from './HomePage/home.component';
import { AccountComponent } from "./account/create.account.component";

export const appRoutes:Routes=[
    { path:'home',component:HomeComponent},
    { path:'login', component:LoginComponent},
    {path:'newuser',component:AccountComponent},
    { path:'', redirectTo:'home',pathMatch:'full'}
]

