import { Component } from "@angular/core";
import {  } from "module";

@Component({
    "moduleId":module.id,
    "selector":'photo-app',
    "template": `
                <div class="row">
                    <div class="col-md-12">
                        <!-- Navigation Panel -->
                        <navigation-bar></navigation-bar>  
                        <router-outlet></router-outlet>                               
                    </div>
                </div>
                `
})

export class PhotoAppComponent {
    // constructor(parameters) {
        
    // }
}