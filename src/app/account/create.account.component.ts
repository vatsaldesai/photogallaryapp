import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    "moduleId":module.id,
    "templateUrl":'./create.account.component.html',
    "styleUrls":[`
            em {float:right; color:#E05C65; padding-left: 10px;}
            .error input {background-color:#dc3545;}
            .error ::-webkit-input-placeholder { color: #dc3545; }
            .error ::-moz-placeholder { color: #dc3545; }
            .error :-moz-placeholder { color:#dc3545; }
            .error :ms-input-placeholder { color: #dc3545; }
    `]
})

export class AccountComponent {

    newEvent: object;
    constructor(private _router:Router) {
        
    }
    saveNewUser(formValues:any){
        console.log(formValues)        
    }
    cancel(){
        this._router.navigate(['/home']);
    }
}